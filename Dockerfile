FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY ./target/educational-system-1-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]