/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.elte.educationalsystem1.controllers;

import edu.elte.educationalsystem1.entities.CustomSubject;
import edu.elte.educationalsystem1.entities.Student;
import edu.elte.educationalsystem1.entities.Subject;
import edu.elte.educationalsystem1.repositories.StudentRepository;
import edu.elte.educationalsystem1.repositories.SubjectRepository;
import edu.elte.educationalsystem1.security.AuthenticatedUser;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Bashar
 */
@RequestMapping("/subjects")
@Controller
public class SubjectsController {
    
    @Autowired 
    private AuthenticatedUser authenticatedUser;
    
    @Autowired
    private StudentRepository studentRepository;
    
    @Autowired
    private SubjectRepository subjectRepository;
    
    @GetMapping("")
    public String index(Model model, @RequestParam(required = false) String subject_name){
     
        if(subject_name != null){
            model.addAttribute("subject_name", subject_name);
        }
        
        model.addAttribute("subjects", subjectRepository.findAllByStudents(authenticatedUser.getStudent()));
        
        return "subjects-list";
    }
    
    @GetMapping("/{id}")
    public String subject_details(Model model,
            @PathVariable Long id){
        
        Optional<Subject> oSubject = subjectRepository.findById(id);
        
        if(oSubject.isPresent()){
            
            Set<Student> students = studentRepository.findAllBySubjects(oSubject.get());
            
            Subject subject = oSubject.get();
            
            subject.setStudents(students);
            
            model.addAttribute(subject);
            return "subject-details";
        }
        
        model.addAttribute("subjects", subjectRepository.findAllByStudents(authenticatedUser.getStudent()));
        
        return "subjects-list";
    }
    
    @GetMapping("/new")
    public String assignmentPage(Model model){
        
        List<CustomSubject> subjects = subjectRepository.getUnassignedSubjects(authenticatedUser.getStudent().getStudentname());
                
        model.addAttribute("subjects", subjects);
        return "assign-new-subject";
    }
    
    @GetMapping("/{id}/new")
    public String assignNewSubject(@PathVariable Long id, Model model){
        
        Optional<Subject> subject = subjectRepository.findById(id);
        
        if(!subject.isPresent()) {
            return "redirect:/subjects/new?error";
        }
        
        Long std_id = authenticatedUser.getStudent().getId();
        
        Integer num_rows_inserted = subjectRepository.enrollStudentInSubject(std_id, subject.get().getId());
        
        if(num_rows_inserted > 0){
                 
            return String.format("redirect:/subjects?subject_name=%s", subject.get().getName());   
        } else{
            return "redirect:/subjects/new?error";
        }
    }
    
    @PostMapping("/{subject_id}/unassign")
    public String unassign(@PathVariable Long subject_id){
        
        Long std_id = authenticatedUser.getStudent().getId();
        
        int num_rows_deleted = subjectRepository.unassignSubject(std_id, subject_id);
        
        return "redirect:/subjects";
    }
}
