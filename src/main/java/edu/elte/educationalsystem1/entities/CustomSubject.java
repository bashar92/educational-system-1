/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.elte.educationalsystem1.entities;

/**
 *
 * @author Bashar
 */
public interface CustomSubject {
    
    int getId();
    String getName();
    String getDescription();
}
