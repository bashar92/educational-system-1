/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.elte.educationalsystem1.repositories;

import edu.elte.educationalsystem1.entities.Student;
import edu.elte.educationalsystem1.entities.Subject;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Bashar
 */
@Repository
public interface StudentRepository extends CrudRepository<Student, Long>{
    Optional<Student> findByStudentname(String studentname);
    
    Set<Student> findAllBySubjects(Subject s);
}
