/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.elte.educationalsystem1.repositories;

import edu.elte.educationalsystem1.entities.CustomSubject;
import edu.elte.educationalsystem1.entities.Student;
import edu.elte.educationalsystem1.entities.Subject;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Bashar
 */
public interface SubjectRepository extends CrudRepository<Subject, Integer>{
    
    List<Subject> findAllByStudents(Student std);
    
    Optional<Subject> findById(Long id);
    
    @Query(
        value="select s.id, s.name, s.description from subject s minus select s.id, s.name, s.description, from subject s join subject_students s_std on s.id = s_std.subjects_id join student std on s_std.students_id = std.id where std.studentname = :std_name"
    ,nativeQuery = true)
    List<CustomSubject> getUnassignedSubjects(@Param("std_name") String std_name);
    
    @Modifying
    @Transactional
    @Query(
        value="INSERT INTO subject_students (students_id, subjects_id) VALUES(:std_id, :subject_id)",
            nativeQuery=true
    )
    Integer enrollStudentInSubject(@Param("std_id") Long std_id, @Param("subject_id") Long subject_id);
    
    @Modifying
    @Transactional
    @Query(
        value="DELETE FROM subject_students WHERE subjects_id = :subject_id AND students_id = :std_id",
            nativeQuery=true
    )
    Integer unassignSubject(@Param("std_id") Long std_id,@Param("subject_id") Long subject_id);
}
