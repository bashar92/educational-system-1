/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.elte.educationalsystem1.security;

import edu.elte.educationalsystem1.entities.Student;
import edu.elte.educationalsystem1.repositories.StudentRepository;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author bashar
 */
@Service
public class MyUserDetailsService implements UserDetailsService {

  @Autowired
  private AuthenticatedUser authenticatedUser;

  @Autowired
  private StudentRepository studentRepository;

  @Override
  public UserDetails loadUserByUsername(String studentname) throws UsernameNotFoundException {
    
    Optional<Student> oStd = studentRepository.findByStudentname(studentname);
    if (!oStd.isPresent()) {
      throw new UsernameNotFoundException(studentname);
    }
    Student std = oStd.get();
    authenticatedUser.setStudent(std);
    Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
    grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_STUDENT"));

    return new org.springframework.security.core.userdetails.User(std.getStudentname(), std.getPassword(), grantedAuthorities);
  }
}
