# 9 Users
INSERT INTO STUDENT (studentname, password, enabled, created_at) VALUES('bashar', '$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', TRUE, CURRENT_TIMESTAMP());
INSERT INTO STUDENT (studentname, password, enabled, created_at) VALUES('alaa', '$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', TRUE, CURRENT_TIMESTAMP());
INSERT INTO STUDENT (studentname, password, enabled, created_at) VALUES('muhammad', '$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', TRUE, CURRENT_TIMESTAMP());
INSERT INTO STUDENT (studentname, password, enabled, created_at) VALUES('mazen', '$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', TRUE, CURRENT_TIMESTAMP());
INSERT INTO STUDENT (studentname, password, enabled, created_at) VALUES('bassel', '$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', TRUE, CURRENT_TIMESTAMP());
INSERT INTO STUDENT (studentname, password, enabled, created_at) VALUES('emad', '$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', TRUE, CURRENT_TIMESTAMP());
INSERT INTO STUDENT (studentname, password, enabled, created_at) VALUES('sohil', '$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', TRUE, CURRENT_TIMESTAMP());
INSERT INTO STUDENT (studentname, password, enabled, created_at) VALUES('lina', '$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', TRUE, CURRENT_TIMESTAMP());
INSERT INTO STUDENT (studentname, password, enabled, created_at) VALUES('jassem', '$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', TRUE, CURRENT_TIMESTAMP());

# 10 Courses
INSERT INTO SUBJECT (name, description, created_at) VALUES('Intro To Data Science', 'Learn the basics behind the hottest Machine Learning Algrithms Nowadays!', CURRENT_TIMESTAMP());
INSERT INTO SUBJECT (name, description, created_at) VALUES('Machine Learning', 'Dig Deeper inside the details of the hottest Machine Learning Algrithms Nowadays!', CURRENT_TIMESTAMP());
INSERT INTO SUBJECT (name, description, created_at) VALUES('Software for advanced ML', 'Learning the hottest Techniques for applying Machine Learning Algrithms Nowadays!', CURRENT_TIMESTAMP());
INSERT INTO SUBJECT (name, description, created_at) VALUES('Interactive Media Design', 'Learn how to be an expert UX designer!', CURRENT_TIMESTAMP());
INSERT INTO SUBJECT (name, description, created_at) VALUES('Numerical Optimization', 'The hottest optimization algorithms used in Machine Learning!', CURRENT_TIMESTAMP());
INSERT INTO SUBJECT (name, description, created_at) VALUES('Software Technology', 'Learning the hottest Software Technologies that most worldwide companies ask for Nowadays!', CURRENT_TIMESTAMP());
INSERT INTO SUBJECT (name, description, created_at) VALUES('Theory of Programming', 'How to be a successful programmer! right bug-free code with this material', CURRENT_TIMESTAMP());
INSERT INTO SUBJECT (name, description, created_at) VALUES('Advanced Programming', 'Learn the modern software architectures used Nowadays in huge applications!', CURRENT_TIMESTAMP());
INSERT INTO SUBJECT (name, description, created_at) VALUES('Computational Models', 'The theory behind most famous computational models!', CURRENT_TIMESTAMP());
INSERT INTO SUBJECT (name, description, created_at) VALUES('Lab Course', 'Let get our hands dirty with real projects!', CURRENT_TIMESTAMP());

INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(1, 1);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(1, 2);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(2, 1);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(2, 3);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(4, 3);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(1, 3);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(4, 5);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(4, 6);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(1, 4);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(6, 2);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(6, 3);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(5, 2);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(6, 1);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(5, 1);
INSERT INTO SUBJECT_STUDENTS (students_id, subjects_id) VALUES(4, 1);